package com.demo.briefapp.service;

import com.demo.briefapp.model.Brief;
import com.demo.briefapp.model.BriefResponse;
import com.itextpdf.text.DocumentException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


class PdfGeneratorTest {

    PdfGenerator pdfGenerator = new PdfGenerator();

    @Test
    public void testGeneratePdf() throws IOException, DocumentException {
        List<BriefResponse> responseList = new ArrayList<>();
        responseList.add(BriefResponse.builder().question("питання 1").answer("відповідь 1").build());
        responseList.add(BriefResponse.builder().question("питання 2").answer("відповідь 2").build());
        responseList.add(BriefResponse.builder().question("питання 3").answer("відповідь 3").build());
        responseList.add(BriefResponse.builder().question("питання 4").answer("відповідь 4").build());

        Brief brief = new Brief();
        brief.setResponses(responseList);

        pdfGenerator.generatePdf(brief);

        File file = new File("brief.pdf");
        Assertions.assertTrue(file.exists());
    }
}