package com.demo.briefapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BriefAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BriefAppApplication.class, args);
	}

}
