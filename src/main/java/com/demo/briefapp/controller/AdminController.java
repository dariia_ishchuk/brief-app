package com.demo.briefapp.controller;

import com.demo.briefapp.model.Brief;
import com.demo.briefapp.service.BriefRepository;
import com.demo.briefapp.service.PdfGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.File;
import java.util.List;

@Log4j2
@RequiredArgsConstructor
@Controller
public class AdminController {

    private final BriefRepository briefRepository;
    private final PdfGenerator pdfGenerator;

    @GetMapping("/admin")
    public String home() {
        return "redirect:admin/dashboard";
    }

    @GetMapping("/admin/dashboard")
    public String showAdminPage(Model model) {
        List<Brief> briefs = briefRepository.findAll();
        model.addAttribute("briefs", briefs);
        return "admin/admin";
    }

    @GetMapping("/admin/{id}")
    public String showBriefDetails(@PathVariable("id") String id, Model model) {
        Brief brief = briefRepository.findById(id).orElse(null);
        model.addAttribute("brief", brief);
        return "admin/brief-details";
    }

    @PostMapping("/admin/edit/{id}")
    public String processBriefUpdate(@PathVariable("id") String id, Brief updatedBrief) {
        Brief existingBrief = briefRepository.findById(id).orElse(null);
        if (existingBrief != null) {
            existingBrief.setResponses(updatedBrief.getResponses());
            briefRepository.save(existingBrief);
        }
        return "redirect:/admin/{id}";
    }

    @GetMapping("/admin/{id}/delete")
    public String deleteBrief(@PathVariable("id") String id) {
        Brief existingBrief = briefRepository.findById(id).orElse(null);
        if (existingBrief != null) {
            briefRepository.delete(existingBrief);
        }
        return "redirect:admin";
    }

    @GetMapping("/admin/{id}/generate-pdf")
    public ResponseEntity<Resource> generatePdf(@PathVariable String id) {
        Brief existingBrief = briefRepository.findById(id).orElse(null);
        if (existingBrief == null) {
            return ResponseEntity.notFound().build();
        }

        File pdfFile = pdfGenerator.generatePdf(existingBrief);
        Resource resource = new FileSystemResource(pdfFile);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("attachment", "brief.pdf");

        return ResponseEntity.ok()
                .headers(headers)
                .body(resource);
    }

    @GetMapping("/admin/login")
    public String loginPage() {
        return "admin/login";
    }
}
