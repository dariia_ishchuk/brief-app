package com.demo.briefapp.controller;

import com.demo.briefapp.model.Brief;
import com.demo.briefapp.model.IncomingBrief;
import com.demo.briefapp.service.BriefRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Log4j2
@RequiredArgsConstructor
@Controller
public class BriefController {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
    private final BriefRepository repository;

    @GetMapping("/")
    public String home() {
        return "redirect:brief";
    }

    @GetMapping({"/brief"})
    public String showBrief(Model model) {
        model.addAttribute("responses", new IncomingBrief());
        return "public/brief";
    }

    @PostMapping("/submit")
    public String submitBrief(@ModelAttribute("incomingBrief") IncomingBrief incomingBrief) {
        Brief brief = new Brief();
        brief.setCreationTime(LocalDateTime.now().format(formatter));
        brief.setResponses(incomingBrief.getResponses());
        repository.save(brief);
        log.info("A new brief has arrived!");
        return "redirect:thank-you";
    }

    @GetMapping("/thank-you")
    public String showThankYou() {
        return "public/thank-you";
    }
}
