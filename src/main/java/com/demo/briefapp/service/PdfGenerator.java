package com.demo.briefapp.service;

import com.demo.briefapp.model.Brief;
import com.demo.briefapp.model.BriefResponse;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.stream.Stream;

@Log4j2
@Service
public class PdfGenerator {

    public static final String FONT = "/fonts/Arial_Regular.ttf";

    public File generatePdf(Brief brief) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("brief.pdf"));

            document.open();

            Font font = FontFactory.getFont(FONT, "cp1251", BaseFont.EMBEDDED, 12);

            Paragraph paragraph = new Paragraph("РЕЗУЛЬТАТИ БРИФІНКУ", font);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            document.add(new Paragraph("\n"));

            PdfPTable table = generateTable(brief, font);

            document.add(table);
            document.close();

            return new File("brief.pdf");
        } catch (DocumentException | IOException dex) {
            log.error("Failed to generate document. Reason [{}].", dex.getMessage());
        }

        return null;
    }

    private PdfPTable generateTable(Brief brief, Font font) {
        PdfPTable table = new PdfPTable(2);
        addTableHeader(table, font);

        for (BriefResponse response : brief.getResponses())
            addRows(table, font, response.getQuestion(), response.getAnswer());

        return table;
    }

    private void addTableHeader(PdfPTable table, Font font) {
        Stream.of("Запитання", "Відповідь")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle, font));
                    table.addCell(header);
                });
    }

    private void addRows(PdfPTable table, Font font, String question, String answer) {
        table.addCell(new PdfPCell(new Phrase(question, font)));
        table.addCell(new PdfPCell(new Phrase(answer, font)));
    }
}
