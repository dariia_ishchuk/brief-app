package com.demo.briefapp.service;

import com.demo.briefapp.model.Brief;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BriefRepository extends MongoRepository<Brief, String> {
}
