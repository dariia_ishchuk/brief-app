package com.demo.briefapp.service;

import com.demo.briefapp.model.Admin;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AdminService implements UserDetailsService {

    private final AdminRepository adminRepository;

    public AdminService(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
        Admin res = this.adminRepository.findByName("admin");
        if (res == null) {
            Admin admin = new Admin();
            admin.setName("admin");
            admin.setPassword("ztuEdu2024");
            adminRepository.save(admin);
        }
    }

    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Admin admin = adminRepository.findByName(name);
        if (admin == null) {
            throw new UsernameNotFoundException("User not found");
        } else {
            return admin;
        }
    }
}
