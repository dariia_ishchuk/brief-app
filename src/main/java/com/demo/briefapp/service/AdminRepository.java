package com.demo.briefapp.service;

import com.demo.briefapp.model.Admin;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AdminRepository extends MongoRepository<Admin, String> {
    Admin findByName(String name);
}