package com.demo.briefapp.config;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CustomUrlFilter implements Filter, Ordered {
    private static final int FILTER_ORDER = SecurityProperties.DEFAULT_FILTER_ORDER - 1;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        // Check if requested URL is valid, if not redirect to /brief
        String requestUri = request.getRequestURI();
        if (!isValidUrl(requestUri)) {
            response.sendRedirect("/brief");
            return;
        }

        filterChain.doFilter(request, response);
    }

    @Override
    public int getOrder() {
        return FILTER_ORDER;
    }

    private boolean isValidUrl(String url) {
        // Check if the URL exists in your application's mappings
        // You may need to implement custom logic to determine this
        return true; // Placeholder logic, replace with your implementation
    }
}
