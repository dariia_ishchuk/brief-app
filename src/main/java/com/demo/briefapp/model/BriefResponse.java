package com.demo.briefapp.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class BriefResponse {

    private String question;

    private String answer;
}
