package com.demo.briefapp.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@Document(collection = "briefs")
public class Brief {

    @Id
    private String id;

    private String creationTime;

    private List<BriefResponse> responses = new ArrayList<>();
}
