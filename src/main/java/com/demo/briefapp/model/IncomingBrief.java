package com.demo.briefapp.model;

import lombok.Data;

import java.util.List;

@Data
public class IncomingBrief {

    private List<BriefResponse> responses;
}
